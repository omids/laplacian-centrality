# Laplacian Centrality
This python script implements Laplacian Centrality for both weighted and unweighted networks.

Laplacian Centrality is a relatively new network centrality measure introduced in 2012 (Qi, Xingqin, et al. "Laplacian centrality: A new centrality measure for weighted networks." Information Sciences 194 (2012): 240-253). The main idea behind this measure is to calculate importance of each node based on the magnitude of the "drop" happening in a quantity called "Laplacian Energy", upon removal of the node from the network.

